import 'package:flutter/material.dart';

class MyHome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(      
       
          title: Container(
            margin: EdgeInsets.only(left:80.0),
            child: Text(
              'Flutter layout demo',
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
              ),        
        ),
          ),
        
      ),
      body: ListView(
        children: [
          Container(
             child: Image.asset(
                        "assets/images/stdio1.jpg",                       
                      ),
          ),
          SizedBox(
            height: 15.0,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                Padding(
                  padding:  EdgeInsets.fromLTRB(0.0,0.0,0.0,5.0),
                  child: Text('Oeschine Lake campground',style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),),
                ),
                Text('Kandersteg, switerland'),
              ],),
              
              Row(children: [
                Icon(
                        Icons.star,
                        size: 20.0,
                        color: Colors.red,
                      ),        
                      Text('41'),      
              ],)
            ],
          ),
          SizedBox(
            height: 30.0,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Column(
               children: [
                 Icon(
                        Icons.phone,
                        size: 25.0,
                        color: Colors.blue,
                      ), 
                      Text('Call')
               ],
              ),
              Column(
               children: [
                 Icon(
                        Icons.phone,
                        size: 25.0,
                        color: Colors.blue,
                      ), 
                      Text('Router')
               ],
              ),
              Column(
               children: [
                 Icon(
                        Icons.phone,
                        size: 25.0,
                        color: Colors.blue,
                      ), 
                      Text('Share')
               ],
              ),
            ],
          ),
          Container(
            child: Padding(
              padding: const EdgeInsets.all(33.0),
              child: Text('Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.'
              ,
              textAlign: TextAlign.left,),
            ),
          ),
          Container(
             child: Image.asset(
                        "assets/images/stdio1.jpg",                       
                      ),
          ),
          SizedBox(
            height: 15.0,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                Padding(
                  padding:  EdgeInsets.fromLTRB(0.0,0.0,0.0,5.0),
                  child: Text('Oeschine Lake campground',style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),),
                ),
                Text('Kandersteg, switerland'),
              ],),
              
              Row(children: [
                Icon(
                        Icons.star,
                        size: 20.0,
                        color: Colors.red,
                      ),        
                      Text('41'),      
              ],)
            ],
          ),
          SizedBox(
            height: 30.0,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Column(
               children: [
                 Icon(
                        Icons.phone,
                        size: 25.0,
                        color: Colors.blue,
                      ), 
                      Text('Call')
               ],
              ),
              Column(
               children: [
                 Icon(
                        Icons.phone,
                        size: 25.0,
                        color: Colors.blue,
                      ), 
                      Text('Router')
               ],
              ),
              Column(
               children: [
                 Icon(
                        Icons.phone,
                        size: 25.0,
                        color: Colors.blue,
                      ), 
                      Text('Share')
               ],
              ),
            ],
          ),
          Container(
            child: Padding(
              padding: const EdgeInsets.all(33.0),
              child: Text('Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.'
              ,
              textAlign: TextAlign.left,),
            ),
          ),
        ],
      ),
    );
  }
}